Rails.application.routes.draw do
  resources :articles do
    post 'comments', to: 'comments#create', as: 'comments'
  end

  root to: redirect("articles")
end
