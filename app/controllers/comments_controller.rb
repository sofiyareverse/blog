class CommentsController < ApplicationController

  def create
    @article = Article.eager_load(:comments).find(params[:article_id])

    respond_to do |format|
      if @new_comment.save
        format.html { redirect_to @article, notice: 'Comment was successfully created.' }
        format.json { render :show, status: :created, location: @new_comment }
      else
        format.html { render template: 'articles/show' }
        format.json { render json: @new_comment.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    def comment_params
      params.require(:comment).permit(:title, :content)
    end
end
