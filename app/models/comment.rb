class Comment < ApplicationRecord
  belongs_to :articles

  validates :title, length: { minimum: 5, maximum: 60}
  validates :content, length: { minimum: 5, maximum: 300}
end
