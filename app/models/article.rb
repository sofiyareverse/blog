class Article < ApplicationRecord
  has_many :comments

  validates :title, length: { minimum: 5, maximum: 60}
  validates :content, length: { minimum: 50, maximum: 600}
end
